--
-- ExoticaX 3D load_asc() Example -- By: Chris Bensler
--

include exoticaX/exoticaX.ew
include exoticaX/ddrawX.ew
include exoticaX/dinputX.ew
include exoticaX/d3dX.ew
include exoticaX/loadASC.e

set_app_title("EX3D_Load3d Demo for ExoticaX v1.7.1")
report_mode("E_Report",1)

constant FPS=999 -- FrameSpeed Lock
constant s_x=640, s_y=480 -- Screen Width and Height
constant Camera_Distance=100.0

sequence Torus       Torus     = load_3d_file("3D_Files/toorus.asc",0.05)
sequence CrossHairs  CrossHairs= load_3d_file("3D_Files/3torus.asc",0.1)
atom Num_Faces       Num_Faces = Torus[1]/3 + CrossHairs[1]/3

integer ZBUFFER

------------------------------------------------------------------------------
-- INIT
   exotica_initX(s_x,s_y,16)

   -- Setup Direct3D
   if create3d_device(1,0) then exotica_abort(4) end if
   if is_zbufferlesshsr() then
    -- kill the 3d device
    if d3d_release() then exotica_abort(1) end if
    if d3d_init() then exotica_abort(1) end if
    -- reinstate the 3d device without zbuffering
    if create3d_device(1,16) then exotica_abort(1) end if
    ZBUFFER = 1
   else
    ZBUFFER = 0
   end if
   if create_viewport(0,0,s_x,s_y,0.0,1.0) then exotica_abort(5) end if

   -- Setup DirectInput
   if keyboard_init(0) then exotica_abort(7) end if

   material=Set_Ambient_Material(material,0.04,0.04,0.04,0.0)
   if set_materials(material) then exotica_abort(8) end if
   if rs_set_ambient_color(make_rgb({128,128,128})) then exotica_abort(9) end if

   matView = poke_floats(matView,14,{Camera_Distance}) -- _43
   if set_transform_view(matView) then exotica_abort(10) end if
   matProj = poke_floats(matProj,0,{2.0}) -- _11
   matProj = poke_floats(matProj,5,{2.0}) -- _22
   if set_transform_projection(matProj) then exotica_abort(12) end if
   if set_transform_world(matWorld) then exotica_abort(13) end if

    -- Enable z-buffering.
   if ZBUFFER then
    if rs_enable_z_buffering() then exotica_abort(13) end if
   end if

    light=Set_Type_Light(light,D3DLIGHT_DIRECTIONAL)
    light=Set_Diffuse_Light(light,0.7,0.7,0.0,0.0)
    light=Set_Position_Light(light,{1.0,-1.0,100.0})
    light=Set_Direction_Light(light,{1.0,-1.0,1.0})

    if set_light(0,light) then exotica_abort(14) end if
    if light_enable(0,1) then exotica_abort(15) end if
    if rs_enable_lighting() then exotica_abort(16) end if
    if rs_antialias_sortindependent() then exotica_abort(17) end if

------------------------------------------------------------------------------
-- MAIN

atom matT,T_inc,T_posY
matT=allocate(64)
mem_copy(matT,matWorld,64)
T_inc = -0.3   T_posY = 0.0
procedure Draw_Torus()
   material=Set_Diffuse_Material(material,0.0,0.3,0.0,0.0) -- green
   if set_materials(material) then exotica_abort(8) end if
   if T_posY <-100.0 or T_posY > 100.0 then T_inc *=-1 end if
   T_posY +=T_inc
   poke(matT+14*4,atom_to_float32(T_posY))
   if set_transform_world(matT) then exotica_abort(9) end if
   if draw_primitive(D3DPT_TRIANGLELIST,D3DFVF_VERTEX,Torus[2],Torus[1]) then
      exotica_abort(16)
   end if
end procedure

atom matC,counter
matC=allocate(64)
mem_copy(matC,matWorld,64)
counter=0.0
procedure Draw_CrossHair()
   material=Set_Diffuse_Material(material,0.5,0.0,0.0,0.0) -- red
   if set_materials(material) then exotica_abort(8) end if
   counter+=0.05
   if (counter>=360.0) then counter-=360.0 end if
   if set_transform_world(poke_floats(matC,0,Set_Rotate_Z_Matrix(counter))) then
      exotica_abort(9)
   end if
   if draw_primitive(D3DPT_TRIANGLELIST,D3DFVF_VERTEX,CrossHairs[2],CrossHairs[1]) then
      exotica_abort(16)
   end if
end procedure

-- Exit program by hitting the escape key
while not keyboard_keystate(DIK_ESCAPE) do
if aActive() then

   -- Update Direct Input for Immediate data(ie:mouse,jouystick,keyboard)
   dinput_update()

   if clear_viewport(or_bits(D3DCLEAR_TARGET,D3DCLEAR_ZBUFFER*ZBUFFER),0, 1.0) then exotica_abort(14) end if

   if begin_scene() then exotica_abort(15) end if

   Draw_Torus()
   Draw_CrossHair()

   if end_scene() then exotica_abort(17) end if


   if gdi_textout(5,5,
	 sprintf("FPS : %03d    VIDEO MEMORY MODE : %d    3D MEMORY MODE : %d    # FACES : %d",
		  {framerate(FPS),get_prim_surface_mem(),get_d3d_memory_mode(),Num_Faces}),{255,255,255},1) then
      exotica_abort(18)
   end if
   --Flip the back buffer surface to the Primary surface to
   --let us see the updated drawings
   if surface_flip() then exotica_abort(19) end if

end if
   if exotica_error() then exotica_abort(20) end if
end while

------------------------------------------------------------------------------
-- EXIT

-- Shutdown EXOTICA. This must be called at the program's end
-- when you have done an EXOTICA_INIT at the beginning of your program.
exotica_exit()

