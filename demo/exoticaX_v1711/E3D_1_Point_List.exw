--
-- Exotica 3D Example -- By: Todd A.Riggins
--
include exoticaX/exoticaX.ew
include exoticaX/ddrawX.ew
include exoticaX/dinputX.ew
include exoticaX/d3dX.ew

set_app_title("E3D_1_POINTSLIST Demo for ExoticaX v1.7.1")
report_mode("E_Report",1)


constant s_x=640, s_y=480-- screen width & height
constant FPS=60 -- FrameSpeed Lock


sequence vnormal  -- vector normal
atom Vertices
Vertices=allocate(192) -- 8 Members of the Vertex * 4 bytes each * 6 Vertices

------------------------------------------------------------------------------
-- INIT

   exotica_init()

   -- Set up Direct Draw
   if ddraw_init(s_x,s_y,16) then exotica_abort(1) end if

   -- Setup DirectInput
   if dinput_init() then exotica_abort(5) end if
   if keyboard_init(0) then exotica_abort(6) end if

   -- Setup Direct3D
   if d3d_init() then exotica_abort(3) end if
   if create3d_device(0,0) then exotica_abort(4) end if
   if create_viewport(0,0,s_x,s_y,0.0,1.0) then exotica_abort(7) end if

   vnormal = { 0.0, 0.0, -1.0 }

   -- Initialize the 3 vertices for the front of the triangle
   Vertices=poke_floats(Vertices,0,{-5.0,-5.0, 0.0} & vnormal & {0.0,0.0} &
       { 0.0, 5.0, 0.0} & vnormal & {0.0,0.0} &
       { 5.0,-5.0, 0.0} & vnormal & {0.0,0.0} &
       {10.0, 5.0, 0.0} & vnormal & {0.0,0.0} &
       {15.0,-5.0, 0.0} & vnormal & {0.0,0.0} &
       {20.0, 5.0, 0.0} & vnormal & {0.0,0.0})

   -- Set the material as yellow. We're setting the ambient color here
   -- since this tutorial only uses ambient lighting. For apps that use real
   -- lights, the diffuse and specular values should be set. (In addition, the
   -- polygons' vertices need normals for true lighting.)
   material=Set_Ambient_Material(material, 1.0, 1.0, 0.0, 0.0)
   if set_materials(material) then exotica_abort(8) end if

   -- The ambient lighting value is another state to set. Here, we are turning
   -- ambient lighting on to full white.
   if rs_set_ambient_color(make_rgb({255,255,255})) then exotica_abort(9) end if

   -- The view matrix defines the position and orientation of the camera.
   -- Here, we are just moving it back along the z-axis by 50 units.
   matView=poke_floats(matView,14,{50.0}) -- set matView_43 to 50.0
   if set_transform_view(matView) then exotica_abort(11) end if

   -- The projection matrix defines how the 3D scene is "projected" onto the
   -- 2D render target (the backbuffer surface). Refer to the docs for more
   -- info about projection matrices.
   matProj=poke_floats(matProj,0,{2.0})
   matProj=poke_floats(matProj,5,{2.0})
   if set_transform_projection(matProj) then exotica_abort(12) end if

------------------------------------------------------------------------------
-- MAIN

atom counter   counter=0.0

-- Exit program by hitting the escape key
while not keyboard_keystate(DIK_ESCAPE) do
if aActive() then

  -- Update Direct Input for Immediate data(ie:mouse,jouystick,keyboard)
  dinput_update()

  counter+=0.01
  if (counter>=360.0) then counter-=360.0 end if

  -- Rotate on the vertical axis
  matWorld=poke_floats(matWorld,0,Set_Rotate_Y_Matrix(counter))
  if set_transform_world(matWorld) then exotica_abort(13) end if

  if clear_viewport(D3DCLEAR_TARGET,make_rgb({0,0,65535}), 0.0) then exotica_abort(14) end if

  if begin_scene() then exotica_abort(15) end if

  -- Draw the triangle using a DrawPrimitive() call. Subsequent
  -- tutorials will go into more detail on the various calls for
  -- drawing polygons.

  if draw_primitive(D3DPT_POINTLIST,D3DFVF_VERTEX,Vertices,6) then
     exotica_abort(16)
  end if

  if end_scene() then exotica_abort(17) end if

  if gdi_textout(5,5,sprintf("FPS : %03d",framerate(FPS)),{255,255,255},1) then
     exotica_abort(18)
  end if

  --Flip the back buffer surface to the Primary surface to
  --let us see the updated drawings
  if surface_flip() then exotica_abort(19) end if

end if
   if exotica_error() then exotica_abort(20) end if
end while

------------------------------------------------------------------------------
-- EXIT

-- Shutdown EXOTICA. This must be called at the program's end
-- when you have done an EXOTICA_INIT at the beginning of your program.
exotica_exit()


