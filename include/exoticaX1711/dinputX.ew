-- dinputX.ew v1.7.1
-- written by Chris Bensler
-- LAST MODIFIED : 01/26/02 / 02:12
---------------------------------------------------------------------------------
without warning
include exoticaX/exoticaX.ew

-- {{MAJOR,MINOR,REVISION},DEBUG}
verify_exoticaX_library({1,7,1},"dinputX.ew")

atom Is_Joystick    Is_Joystick=1

-----------------------------------------------------------------------------
-- DINPUT ROUTINES --

global function dinput_init()
if Xget_is_initX(2) then
   error_message("DINPUT_INIT already initialized")
   return 1
else
 Xset_is_initX(2,1)
 if c_func(DINPUT_INIT,{}) then
    error_message("DINPUT_INIT")
    return 1
 end if
 return 0
end if
end function
Xset_exoticaX_id(2,routine_id("dinput_init"))

global procedure dinput_release()
Xset_is_initX(2,0)
 c_proc(DINPUT_RELEASE,{})
end procedure

global procedure reaquire_input()
 c_proc(REAQUIRE_INPUT,{})
end procedure

global procedure dinput_update()
 c_proc(DINPUT_UPDATE,{})
end procedure

-----------------------------------------------------------------------------
-- KEYBOARD ROUTINES --

global function keyboard_init(integer kb_MODE)
 if c_func(KEYBOARD_INIT,{kb_MODE}) then
    error_message("KEYBOARD_INIT")
    return 1
 end if
 return 0
end function

global procedure keyboard_release()
 c_proc(KEYBOARD_RELEASE,{})
end procedure

global function keyboard_keystate(atom Key_Value)
 return c_func(KEYBOARD_KEYSTATE,{Key_Value})
end function

global function get_kb_buffer_events()
 return c_func(GET_KB_BUFFER_EVENTS,{})
end function

global function get_kb_buffer_data(atom element, atom DIK_KEY)
 return c_func(GET_KB_BUFFER_DATA,{element, DIK_KEY})
end function

-----------------------------------------------------------------------------
-- MOUSE ROUTINES --

global function mouse_init()
 if c_func(MOUSE_INIT,{}) then
    error_message("MOUSE_INIT")
    return 1
 end if
 -- clear the surface so you don't see the video memory dump
 aActive_wait()
 if c_func(CLEAR_SURFACE,{#000000}) then exotica_abort(-6) end if
 if c_func(SURFACE_FLIP,{}) then exotica_abort(-7) end if
 return 0
end function

global procedure mouse_release()
 c_proc(MOUSE_RELEASE,{})
end procedure

global function mouse_x()
 return c_func(MOUSE_X,{})
end function

global function mouse_y()
 return c_func(MOUSE_Y,{})
end function

global function mouse_z()
 return c_func(MOUSE_Z,{})
end function

global function mouse_b0()
 return c_func(MOUSE_B0,{})
end function

global function mouse_b1()
 return c_func(MOUSE_B1,{})
end function

global function mouse_b2()
 return c_func(MOUSE_B2,{})
end function

global function mouse_b3()
 return c_func(MOUSE_B3,{})
end function

global function mouse_b4()
 return c_func(MOUSE_B4,{})
end function

global function mouse_b5()
 return c_func(MOUSE_B5,{})
end function

global function mouse_b6()
 return c_func(MOUSE_B6,{})
end function

global function mouse_b7()
 return c_func(MOUSE_B7,{})
end function

-----------------------------------------------------------------------------
-- JOYSTICK ROUTINES --

global function joystick_init()
atom tmp
 tmp = c_func(JOYSTICK_INIT,{})
 if tmp = 1 then
    error_message("JOYSTICK_INIT")
    return 1
 end if
 if tmp = -1 then Is_Joystick = 0 end if
 return 0
end function

global procedure joystick_release()
 c_proc(JOYSTICK_RELEASE,{})
end procedure

global function joystick_detected()
 return Is_Joystick
end function

global procedure joystick_control_panel()
 c_proc(JOYSTICK_CONTROL_PANEL,{})
end procedure

global procedure joy_x_range(atom min,atom max)
 c_proc(JOY_X_RANGE,{min, max })
end procedure

global procedure joy_x_deadzone(atom zone)
 c_proc(JOY_X_DEADZONE,{ zone })
end procedure

global procedure joy_x_saturation(atom sat)
 c_proc(JOY_X_SATURATION,{ sat })
end procedure

global procedure joy_y_range(atom min,atom max)
 c_proc(JOY_Y_RANGE,{min, max })
end procedure

global procedure joy_y_deadzone(atom zone)
 c_proc(JOY_Y_DEADZONE,{ zone })
end procedure

global procedure joy_y_saturation(atom sat)
 c_proc(JOY_Y_SATURATION,{ sat })
end procedure

global procedure joy_z_range(atom min,atom max)
 c_proc(JOY_Z_RANGE,{min, max })
end procedure

global procedure joy_z_deadzone(atom zone)
 c_proc(JOY_Z_DEADZONE,{ zone })
end procedure

global procedure joy_z_saturation(atom sat)
 c_proc(JOY_Z_SATURATION,{ sat })
end procedure

global procedure joy_rx_range(atom min,atom max)
 c_proc(JOY_RX_RANGE,{min, max })
end procedure

global procedure joy_rx_deadzone(atom zone)
 c_proc(JOY_RX_DEADZONE,{ zone })
end procedure

global procedure joy_rx_saturation(atom sat)
 c_proc(JOY_RX_SATURATION,{ sat })
end procedure

global procedure joy_ry_range(atom min,atom max)
 c_proc(JOY_RY_RANGE,{min,max})
end procedure

global procedure joy_ry_deadzone(atom zone)
 c_proc(JOY_RY_DEADZONE,{ zone })
end procedure

global procedure joy_ry_saturation(atom sat)
 c_proc(JOY_RY_SATURATION,{ sat })
end procedure

global procedure joy_rz_range(atom min,atom max)
 c_proc(JOY_RZ_RANGE,{min,max})
end procedure

global procedure joy_rz_deadzone(atom zone)
 c_proc(JOY_RZ_DEADZONE,{ zone })
end procedure

global procedure joy_rz_saturation(atom sat)
 c_proc(JOY_RZ_SATURATION,{ sat })
end procedure

global procedure joy_slider_range(atom slider, atom min, atom max)
 c_proc(JOY_SLIDER_RANGE,{ slider, min, max })
end procedure

global function joy_x_axis()
 return c_func( JOY_X_AXIS,{})
end function

global function joy_y_axis()
 return c_func( JOY_Y_AXIS,{})
end function

global function joy_z_axis()
 return c_func( JOY_Z_AXIS,{})
end function

global function joy_rx_axis()
 return c_func( JOY_RX_AXIS,{})
end function

global function joy_ry_axis()
 return c_func( JOY_RY_AXIS,{})
end function

global function joy_rz_axis()
 return c_func( JOY_RZ_AXIS,{})
end function

global function joy_slider(atom slider)
 return c_func( JOY_SLIDER,{slider})
end function

global function joy_pov(atom povhat)
 return c_func( JOY_POV,{povhat})
end function

global function joy_button(atom but_NUM)
 return c_func(JOY_BUTTON,{but_NUM})
end function

-----------------------------------------------------------------------------

