-- ewin32api.ew for use with the
-- EXOTICA dll
-- (c)1999 Todd Riggins
--
-- This file sets up needed win32 functions and variables for use with the
-- EXOTICA dll.

include machine.e
include dll.e
include get.e
include msgbox.e

global function or_all(sequence s)
    -- or together all elements of a sequence
    atom result
    
    result = 0
    for i = 1 to length(s) do
	  result = or_bits(result, s[i])
    end for
    return result
end function

-- Structure of WNDCLASS
global constant cbSize    = 0,
         style            = 4,
         lpfnWndProc      = 8,
         cbClsExtra       = 12,
         cbWndExtra       = 16,
         hInstance        = 20,
         hIcon            = 24,
         hCursor          = 28,
         hbrBackground    = 32,
         lpszMenuName     = 36,
         lpszClassName    = 40,
         hIconSm          = 44,
         SIZE_OF_WNDCLASS = 48

global constant SIZE_OF_MESSAGE = 40

-- Class styles
global constant CS_HREDRAW = 2,
                CS_VREDRAW = 1

-- ShowWindow() Commands
global constant SW_SHOW = 5

-- GetSystemMetrics() codes
global constant SM_CXSCREEN = 0,
                SM_CYSCREEN = 1

-- PeekMessage() Options
global constant PM_NOREMOVE = #0000

-- Stock Logical Objects
global constant NULL_BRUSH = 5


-- Window Messages
global constant WM_DESTROY     = #0002,
                WM_ACTIVATEAPP = #001C,
                WM_SETCURSOR   = #0020,
                WM_KEYDOWN     = #0100,
                WM_MOUSEMOVE   = #0200

-- Virtual Keys, Standard Set
global constant VK_ESCAPE = #1B

-- PALETTEENTRY flags 

global constant PRESERVED     = #01    -- palette index used for animation 
global constant PNOCOLLAPSE   = #04    -- do not match color to system palette 

-- EXOTICA needs both PRESERVED and PNOCOLLAPSE, so I or_all them
-- into a specially needed flag for easy use
global constant PE_FLAGS = or_all({PRESERVED, PNOCOLLAPSE})


-- Window Styles
global constant WS_OVERLAPPED  = #00000000,
                WS_POPUP       = #80000000,
                WS_CAPTION     = #00C00000,   
  	             WS_SYSMENU     = #00080000,
	             WS_THICKFRAME  = #00040000,
	             WS_MINIMIZEBOX = #00020000,
	             WS_MAXIMIZEBOX = #00010000,
                WS_OVERLAPPEDWINDOW = or_all({WS_OVERLAPPED, WS_CAPTION, WS_SYSMENU,
                                        WS_THICKFRAME, WS_MINIMIZEBOX, WS_MAXIMIZEBOX})

-- Extended Window Styles
global constant WS_EX_TOPMOST = #00000008

-- CreateFont Weight values
global constant 
	FW_DONTCARE	=		0,
	FW_THIN =			100,
	FW_EXTRALIGHT =		200,
	FW_LIGHT =			300,
	FW_NORMAL =			400,
	FW_MEDIUM =			500,
	FW_SEMIBOLD =		600,
	FW_BOLD =			700,
	FW_EXTRABOLD =		800,
	FW_HEAVY =			900

-- CreateFont Character Set 
global constant
 ANSI_CHARSET          =  0,
 DEFAULT_CHARSET       =  1,
 SYMBOL_CHARSET        =  2,
 SHIFTJIS_CHARSET      =  128,
 HANGEUL_CHARSET       =  129,
 HANGUL_CHARSET        =  129,
 GB2312_CHARSET        =  134,
 CHINESEBIG5_CHARSET   =  136,
 OEM_CHARSET           =  255,

 JOHAB_CHARSET         =  130,
 HEBREW_CHARSET        =  177,
 ARABICHARSET        =  178,
 GREEK_CHARSET         =  161,
 TURKISH_CHARSET       =  162,
 VIETNAMESE_CHARSET    =  163,
 THAI_CHARSET          =  222,
 EASTEUROPE_CHARSET    =  238,
 RUSSIAN_CHARSET       =  204,
 MACHARSET           =  77,
 BALTICHARSET        =  186

-- CreateFont Output Precision
global constant
 OUT_DEFAULT_PRECIS        =  0,
 OUT_STRING_PRECIS         =  1,
 OUT_CHARACTER_PRECIS      =  2,
 OUT_STROKE_PRECIS         =  3,
 OUT_TT_PRECIS             =  4,
 OUT_DEVICE_PRECIS         =  5,
 OUT_RASTER_PRECIS         =  6,
 OUT_TT_ONLY_PRECIS        =  7,
 OUT_OUTLINE_PRECIS        =  8,
 OUT_SCREEN_OUTLINE_PRECIS =  9

-- CreateFont Clip Precision
global constant
 CLIP_DEFAULT_PRECIS     = 0,
 CLIP_CHARACTER_PRECIS   = 1,
 CLIP_STROKE_PRECIS      = 2,
 CLIP_MASK               = #F,
 CLIP_LH_ANGLES          = 16,
 CLIP_TT_ALWAYS          = 32,
 CLIP_EMBEDDED           = 128

-- CreateFont Quality
global constant
 DEFAULT_QUALITY       =  0,
 DRAFT_QUALITY         =  1,
 PROOF_QUALITY         =  2
-- or_bits() with one of the following...
global constant
 DEFAULT_PITCH         =  0,
 FIXED_PITCH           =  1,
 VARIABLE_PITCH        =  2

-- CreateFont Pitch and Family
global constant
 FF_DONTCARE         = 0,   -- (0<<4)  /* Don't care or don't know. */
 FF_ROMAN            = 16,  -- (1<<4)  /* Variable stroke width, serifed. */
                            --         /* Times Roman, Century Schoolbook, etc. */
 FF_SWISS            = 32,  -- (2<<4)  /* Variable stroke width, sans-serifed. */
                            --         /* Helvetica, Swiss, etc. */
 FF_MODERN           = 240, -- (3<<4)  /* Constant stroke width, serifed or sans-serifed. */
                            --         /* Pica, Elite, Courier, etc. */
 FF_SCRIPT           = 64,  -- (4<<4)  /* Cursive, etc. */
 FF_DECORATIVE       = 80   -- (5<<4)  /* Old English, etc. */

------------------------------------------------------
--- Global Stuff Needed from Microsoft's Win32 API ---
------------------------------------------------------   
-- Setup integers
global integer GetSystemMetrics, PeekMessage, SetCursor, LoadIcon,
        LoadCursor, GetStockObject, RegisterClassEx,
        CreateWindow, ShowWindow, UpdateWindow,
        TranslateMessage, DispatchMessage, PostQuitMessage,
        GetMessage, PostMessage, DefWindowProc
-- user functions/procedures
global integer timeGetTime

-- kernel32 routines
global integer GetTickCount

-- gdi32 routines
global integer CreateFont

procedure not_found()--sequence name)
--        funcval = message_box("Couldn't find " & name, "FAILED!!!", MB_ICONINFORMATION+MB_TASKMODAL ) 
  abort(1)
end procedure

function link_func(atom dll, sequence name, sequence args, atom result)
-- dynamically link a C routine as a Euphoria function
 integer handle

  handle = define_c_func(dll, name, args, result)
  if handle = -1 then
	  not_found()--name)
  else
  	return handle
  end if
end function

function link_proc(atom dll, sequence name, sequence args)
-- dynamically link a C routine as a Euphoria function
 integer handle

  handle = define_c_proc(dll, name, args)
  if handle = -1 then
  	not_found()--name)
  else
	  return handle
  end if
end function

procedure link_win32_dll_routines()
-- get handles to all dll routines that we need
 atom user32, gdi32, winmm, kernel32

  user32 = open_dll("user32.dll")
  if user32 = NULL then
  	not_found()--"user32.dll")
  end if
  gdi32 = open_dll("gdi32.dll")
  if gdi32 = NULL then
	  not_found()--"gdi32.dll")
  end if
  winmm = open_dll("winmm.dll")
  if winmm = NULL then
	  not_found()--"winmm.dll")
  end if
  kernel32 = open_dll("kernel32.dll")
  if kernel32 = NULL then
	  not_found()--"kernel32.dll")
  end if
-----------------------------------------------
--- Stuff Needed from Microsoft's Win32 API ---
-----------------------------------------------   
  GetSystemMetrics = link_func(user32, "GetSystemMetrics", {C_INT}, C_INT)
  PeekMessage = link_func(user32, "PeekMessageA", {C_ULONG,C_ULONG,C_UINT,C_UINT,C_UINT}, C_INT)
  SetCursor = link_func(user32, "SetCursor", {C_ULONG}, C_ULONG)
  timeGetTime = link_func(winmm,"timeGetTime",{}, C_ULONG)
  LoadIcon = link_func(user32, "LoadIconA", {C_ULONG, C_INT}, C_INT)
  LoadCursor = link_func(user32, "LoadCursorA", {C_ULONG, C_INT}, C_INT)
  GetStockObject = link_func(gdi32, "GetStockObject", {C_INT}, C_INT)
  RegisterClassEx = link_func(user32, "RegisterClassExA", {C_ULONG}, C_INT)
  CreateWindow = link_func(user32, "CreateWindowExA", 
        {C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT,C_INT},
        C_INT)
  ShowWindow = link_proc(user32, "ShowWindow", {C_INT, C_INT})
  UpdateWindow = link_proc(user32, "UpdateWindow", {C_INT})
  GetMessage = link_func(user32, "GetMessageA",{C_INT, C_INT, C_INT, C_INT}, C_INT)
  TranslateMessage = link_proc(user32, "TranslateMessage", {C_INT})
  DispatchMessage = link_proc(user32, "DispatchMessageA", {C_INT})
  PostQuitMessage = link_proc(user32, "PostQuitMessage", {C_INT})
  PostMessage = link_func(user32, "PostMessageA", {C_INT, C_UINT, C_INT, C_INT },C_INT)
  DefWindowProc = link_func(user32, "DefWindowProcA",{C_INT, C_INT, C_INT, C_INT}, C_INT)
  GetTickCount = link_func(kernel32,"GetTickCount",{},C_ULONG)
  CreateFont = link_func(gdi32,"CreateFontA",
        {C_INT,C_INT,C_INT,C_INT,C_INT,C_ULONG,C_ULONG,C_ULONG,C_ULONG,C_ULONG,C_ULONG,C_ULONG,C_ULONG,C_ULONG},
        C_ULONG)
  
end procedure

link_win32_dll_routines()

