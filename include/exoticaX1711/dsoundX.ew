-- dsoundX.ew v1.7.1
-- written by Chris Bensler
-- LAST MODIFIED : 01/26/02 / 02:12
---------------------------------------------------------------------------------
include exoticaX/exoticaX.ew

-- {{MAJOR,MINOR,REVISION},DEBUG}
verify_exoticaX_library({1,7,1},"dsoundX.ew")

atom NUM_SNDS  NUM_SNDS=0
atom SND_FAIL  SND_FAIL=0

------------------------------------------------------------------------}
-- DSOUND ROUTINES --{--}

global function dsound_init(atom ds_MODE,integer ds_CHANNELS,integer ds_FREQ,integer ds_BPS)--{
if Xget_is_initX(3) then
   error_message("DSOUND_INIT already initialized")
   return 1
else
 Xset_is_initX(3,1)
 if c_func(DSOUND_INIT,{ds_MODE, ds_CHANNELS, ds_FREQ, ds_BPS}) then
   return 1
 end if
 return 0
end if
end function
Xset_exoticaX_id(3,routine_id("dsound_init"))

global function dsound_release()
Xset_is_initX(3,0)
 return c_func(DSOUND_RELEASE,{})
end function--                                                                                }

------------------------------------------------------------------------
-- (WAV) SOUND ROUTINES --{--}

global function load_sound(sequence wav,atom wav_BUFFERS,atom wav_MODE)--{
atom tmp
tmp = allocate_string(wav)
   NUM_SNDS +=1
   if c_func( LOAD_SOUND,{ tmp,NUM_SNDS,wav_BUFFERS, wav_MODE }) then
      error_message(sprintf("LOAD_SOUND (%s)",{wav}))
      free(tmp)
      NUM_SNDS -=1
      SND_FAIL=1
      return 0
   end if
   free(tmp)
   return NUM_SNDS
end function--                                                           }

global function load_sound_failed()--{
 return SND_FAIL
end function--                       }

global procedure free_sound(atom wav_ID)--{
  c_proc(FREE_SOUND,{wav_ID})    
end procedure--                           }

global function play_sound(atom wav_ID,atom wav_VOL,atom wav_PAN,
                                                atom wav_FREQ,atom wav_LOOP)--{
   if c_func( PLAY_SOUND,{wav_ID, wav_VOL,wav_PAN,wav_FREQ,wav_LOOP}) then
      error_message(sprintf("PLAY_SOUND (%d)",wav_ID))
      return 1
   end if
   return 0
end function--                                                                }

global procedure set_vol(atom wav_ID,atom wav_VOL)--{
 c_proc(SET_VOL,{wav_ID,wav_VOL})
end procedure--                                     }

global procedure set_pan(atom wav_ID,atom wav_PAN)--{
 c_proc(SET_PAN,{wav_ID,wav_PAN})   
end procedure--                                     }

global procedure set_freq(atom wav_ID,atom wav_FREQ)--{
 c_proc(SET_FREQ,{wav_ID,wav_FREQ})   
end procedure--                                       }

global function get_free_dshwbuffers()--{
   return c_func(FREE_DSHWBUFFERS,{})
end function--                          }

global function get_free_dshwbytes()--{
   return c_func(FREE_DSHWBYTES,{})
end function--                        }

global function total_dshwbytes()--{
   return c_func(TOTAL_DSHWBYTES,{})
end function--                     }

global function get_pan(atom wav_ID)--{
   return c_func(GET_PAN,{wav_ID})
end function--                        }

global function get_vol(atom wav_ID)--{
   return c_func(GET_VOL,{wav_ID})
end function--                        }

global function get_freq(atom wav_ID)--{
   return c_func(GET_FREQ,{wav_ID})
end function--                         }

global function get_swfreq_min()--{
   return c_func(GET_SWFREQ_MIN,{})
end function--                    }

global function get_swfreq_max()--{
   return c_func(GET_SWFREQ_MAX,{})
end function--                    }

global function get_hwfreq_min()--{
   return c_func(GET_HWFREQ_MIN,{})
end function--                    }

global function get_hwfreq_max()--{
   return c_func(GET_HWFREQ_MAX,{})
end function--                    }


------------------------------------------------------------------------
-- CD ROUTINES --{
atom CD_Status CD_Status = 0
--               }

global procedure cd_init()--{
 c_proc(CD_OPEN,{})
 HOT_CD = 1
end procedure--             }

global procedure cd_close()--{
 c_proc(CD_CLOSE,{})
 HOT_CD = 0
end procedure--              }

global procedure cd_play()--{
 c_proc(CD_PLAY,{})
 CD_Status = 1
end procedure--             }

global procedure cd_playtrack(atom cd_TRACK)--{
 c_proc(CD_PLAYTRACK,{cd_TRACK})
 CD_Status = 1
end procedure--                               }

global procedure cd_stop()--{
 c_proc(CD_STOP,{})
 CD_Status = 0
end procedure--             }

global procedure cd_pause()--{
 c_proc(CD_PAUSE,{})
 CD_Status = 2
end procedure--              }

global procedure cd_resume()--{
 c_proc(CD_RESUME,{})
 CD_Status = 1
end procedure--               }

global procedure cd_trackprevious()--{
 c_proc(CD_TRACKPREVIOUS,{})
 CD_Status = 1
end procedure--                      }

global procedure cd_tracknext()--{
 c_proc(CD_TRACKNEXT,{})
 CD_Status = 1
end procedure--                  }

global function cd_getnumtracks()--{
 return c_func(CD_GETNUMTRACKS,{})   
end function--                     }

global function cd_getcurrenttrack()--{
 return c_func(CD_GETCURRENTTRACK,{})    
end function--                        }

--{v1.7
-- TRACK:MIN:SEC:MILLI
--}
global function cd_getposition()--{
   return peek({c_func(CD_GETPOSITION,{}),11})
end function--                    }

--{v1.7
-- MIN:SEC:MILLI
--}
global function cd_gettrack_length(integer Track)--{
   return peek({c_func(CD_GETTRACK_LENGTH,{Track}),8})
end function--                                     }

--{v1.7
-- MIN:SEC:MILLI
--}
global function cd_getlength()--{
   return peek({c_func(CD_GETLENGTH,{}),8})
end function--                  }

--{v1.7
--  0=NOT PLAYING
--  1=PLAYING
--  2=PAUSED
--}
global function cd_status()--{
   return CD_Status
end function--               }

--{v1.7}
global function cd_ready()--{
atom tmp
   tmp = c_func(CD_GETSTATUS,{})
   if tmp = 0 or tmp = 5 then return 0 else return 1 end if
end function--              }
 
------------------------------------------------------------------------
-- DMUSIC ROUTINES --{--}

global function dmusic_init()--{
   if c_func(DMUSIC_INIT,{}) then
      error_message("DMUSIC INIT FAILED")
      return 1
   end if
   return 0
end function--                 }

global procedure dmusic_release()--{
   c_proc(DMUSIC_RELEASE,{})
end procedure--                    }

global procedure dmusic_toggle_reverb(atom toggle)--{
   c_proc(DMUSIC_TOGGLE_REVERB,{toggle})
end procedure--                                     }

global function dmusic_reverb_avail()--{
   return c_func(DMUSIC_REVERB_AVAIL,{})
end function--                         }

------------------------------------------------------------------------
-- MIDI ROUTINES --{--}

global procedure midi_open(sequence mid)--{
atom tmp
tmp = allocate_string(mid)
 c_proc(MIDI_OPEN,{tmp})
 free(tmp)
 HOT_MIDI = 1
end procedure--                           }

global procedure midi_close()--{
 c_proc(MIDI_STOP,{}) 
 c_proc(MIDI_CLOSE,{})
 HOT_MIDI = 0
end procedure--                }

global procedure midi_play()--{
 c_proc(MIDI_PLAY,{})   
end procedure--               }

global procedure midi_stop()--{
 c_proc(MIDI_STOP,{})   
end procedure--               }

global procedure midi_pause()--{
 c_proc(MIDI_PAUSE,{})
end procedure--                }

global procedure midi_resume()--{
 c_proc(MIDI_RESUME,{})   
end procedure--                 }

global function is_midi_ready()--{
 return c_func(IS_MIDI_READY,{})
end function--                   }

------------------------------------------------------------------------
-- (WAV) STREAMING ROUTINES --{--}

global function setup_stream_buffer(sequence wav,atom wav_LOOP)--{
atom tmp
tmp = allocate_string(wav)
  NUM_SNDS +=1
  if c_func( SETUP_STREAM_BUFFER,{ tmp,NUM_SNDS,wav_LOOP }) then
     error_message(sprintf("SETUP_STREAM_BUFFER (%s)",{wav}))
     free(tmp)
     SND_FAIL=1
     NUM_SNDS -=1
     return 0
  end if
  HOT_STREAM = 1
  return NUM_SNDS
end function--                                                   }

global procedure stream_buffer_exit()--{
 c_proc(STREAM_BUFFER_EXIT,{})
 HOT_STREAM = 0
end procedure--                        }

global procedure stream_buffer_update()--{
 c_proc(STREAM_BUFFER_UPDATE,{})   
end procedure--                          }

global procedure stream_buffer_play(atom wav_ID)--{
 c_proc(STREAM_BUFFER_PLAY,{wav_ID})   
end procedure--                                   }

global procedure stream_buffer_stop(atom wav_ID)--{
 c_proc(STREAM_BUFFER_STOP,{wav_ID})   
end procedure--                                   }

global procedure stream_buffer_resume(atom wav_ID)--{
 c_proc(STREAM_BUFFER_RESUME,{wav_ID})   
end procedure--                                     }

global procedure stream_buffer_volume(atom wav_ID,atom wav_VOL)--{
 c_proc(STREAM_BUFFER_VOLUME,{wav_ID,wav_VOL})   
end procedure--                                                  }

global procedure free_streamwave(atom wav_ID)--{
 c_proc( FREE_STREAMWAVE,{wav_ID})
end procedure--                                }

------------------------------------------------------------------------

