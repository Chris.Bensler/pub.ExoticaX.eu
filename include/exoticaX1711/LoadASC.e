-- currently, this lib only supports on object, the first one it sees,
--   it doesn't handle cameras, lights, or materials
--   normals are loaded as identicals to the vertices
-- some of the vertices are duplicated, in order to eliminate the need for faces.
--
-- there is only one global routine in this lib.
--
--function load_3d_file(sequence filename,atom scale)
--   filename is the ASC file you want to load
--   scale is the scale in which to load the vertices
--   
-- returns a sequence : {atom Num_Vertices,atom LP_Vertices, sequence Vertex_List}
--   Num_Vertices is the number of vertices in the object
--   LP_Vertices is a pointer to the memory location of the Vertices
--   Vertex_List is a sequence containg all the vertices, so you don't have to peek memory all the time,
--        or you can use it as a default list, for poking the Vertices into memory


-------------------------------
-- loadasc.e
-------------------------------
include wildcard.e
include file.e
include get.e

integer file_3d
object gl

procedure getline()
   gl = upper(gets(file_3d))
end procedure

function get_number(sequence key)
 integer p1,p2
 sequence v
   p1 = match(key,gl)+length(key)
   p2 = p1
   while gl[p2] = '-' or gl[p2] = '+' or (gl[p2] >= 48 and gl[p2] <= 57) or
		gl[p2] = '.' do
	 p2 +=1
   end while
   v = value(gl[p1..p2-1])
   return v[2]
end function

function find_key(sequence key)
   while (not match(key,gl)) do
      getline()
   end while
   if atom(gl) and gl = -1 then return 0 else return -1 end if    -- 0 = key found
end function

function load_asc(atom scale)
atom Num_Vertices,Num_Faces
sequence Vertices,Vertex_List
integer index
   getline()
   if not find_key("VERTICES:") then return 0 end if  -- not an ASC file
   Num_Vertices = get_number("VERTICES: ")   -- get the # vertices
   Vertex_List = repeat({},Num_Vertices)
   Num_Faces = get_number("FACES: ")      -- get the # faces
   Vertices =repeat({},Num_Faces*3)
   if not find_key("VERTEX LIST") then return {} end if  -- vertex list not found
   for i = 1 to Num_Vertices do
      getline()
      if not find_key("VERTEX") then return {} end if -- vertex list not found
      index = get_number("VERTEX ")+1
      if (index < 1) or (index > Num_Vertices) then return 0 end if -- invalid asc file
      Vertex_List[index]= scale*{get_number("X: "),
				 get_number("Y: "),
				 get_number("Z: ")}
   end for
   if not find_key("FACE LIST") then return {} end if -- face list not found
   for i = 1 to Num_Faces do
      getline()
      if not find_key("FACE") then return {} end if   -- face not found
      index = get_number("FACE ") +1
      if (index < 1) or (index > Num_Faces) then return {} end if --invalid asc file
      Vertices[index*3-2] = Vertex_List[get_number("A:")+1]
      Vertices[index*3-1] = Vertex_List[get_number("B:")+1]
      Vertices[index*3]   = Vertex_List[get_number("C:")+1]
   end for
   return Vertices     -- // everything ok
end function

global function load_3d_file(sequence filename,atom scale)
 atom Num_Vertices,Vertices
 sequence Vertex_List,file_3d_ext
 object retVal
   retVal = {0,0,0}
   file_3d = open(filename,"r")           -- /* read, text */
   file_3d_ext = upper(filename[length(filename)-2..length(filename)])
   if file_3d != -1 then
      if equal(file_3d_ext,"ASC") then
	 Vertex_List = load_asc(scale)
	 Num_Vertices = length(Vertex_List)
	 Vertices = allocate(8*4*Num_Vertices)
	 for i = 1 to Num_Vertices do
	    Vertices=poke_floats(Vertices,(i-1)*8,Vertex_List[i] & Vertex_List[i] & {0.0,0.0})
	 end for
	 retVal = {Num_Vertices,Vertices,Vertex_List}
      end if
   end if
   close(file_3d)
   return retVal
end function

-------------------------------
-- end loadasc.e
-------------------------------

