-------------------------------------------------------------------------------------------------
 
EXOTICAX Version 1.7.11 BY Chris B. Bensler
A wrapper API for Exotica for Euphoria!

The version number for exoticaX just skipped from v1.2 to v1.7 so that the exoticaX version would reflect the required exotica.dll version number.

If you have any problems using ExoticaX for Exotica, be sure to let me know!

                bensler@mail.com


ExoticaX for Exotica is 100% free!

terms of use:

-This must be included in your program and documentation:

                  ExoticaX (c)2000 - 2002 Chris B. Bensler
                  Exotica (c)1999 - 2002 Todd A. Riggins    -- (this is required to use Exotica)

-It would also be nice to get a copy of any programs made using ExoticaX. :)


disclaimer:
-These routines work on my system, however, I cannot guarantee they will
 work on yours. I will not be held responsible for any damage these routines
 may cause to your system. Although I don't see any reason they would.

-- Chris B. Bensler                bensler@mail.com
 
-------------------------------------------------------------------------------------------------
-- Advantages of ExoticaX for Exotica

. No more MainLoop functions
. No more allocating
. Improved release of exotica objects
. Much less coding
. Error_messages are embedded in the ExoticaX routines
. Automatic enumeration of video modes included in ddraw_init()
. Added gdi_create_scroll_text() and gdi_scroll_text() routines
. Just as fast as the exotica routines(no benchmarks, anybody wanna do any?)
 

------------------------------------------------------------------------------------------------
-- Special Thanks to...

- Todd Riggins for his exotica routines and numerous hours of help
- Mark Honor for various routines I ...took from him {:o)
- RDS for providing a top notch interpeter
- Euman and Sid Sidebottom for beta testing